package MathMagick;

public class SummOf {
    private String first = "0.1";
    private String second = "0.15";
    private String third = " 0.25";
    private double dobFirst = Double.valueOf(first);
    private double dobSecond = Double.valueOf(second);
    private double dobThird = Double.valueOf(third);

    private boolean isEquals(double dobFirst, double dobSecond, double dobThird, double eps) {
        return java.lang.Math.abs((dobFirst + dobSecond) - dobThird) <= eps;
    }
    public void printEqquals(){
        System.out.println("Равенство: " + isEquals(dobFirst, dobSecond, dobThird, 1E-5));
    }


}
