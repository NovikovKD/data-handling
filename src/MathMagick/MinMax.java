package MathMagick;

public class MinMax {
    private int first = 3;
    private int second = 46;
    private int third = 25;
    private int max1 = java.lang.Math.max(first,second);
    private int max2 = java.lang.Math.max(second, third);
    private int max3 = java.lang.Math.max(max1, max2);

    public void printMax(){
        System.out.println("Среди трех чисел 3, 46, 25 самым большим оказалось: " + max3);
    }
}
