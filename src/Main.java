import Data.Birthday;
import Data.ConvertData;
import Data.TwoData;
import MathMagick.Math;
import MathMagick.MinMax;
import MathMagick.SummOf;

public class Main {
    public static void main(String[] args) {
        Birthday birthday = new Birthday();
        birthday.printBirthday();

        TwoData data = new TwoData();
        data.printTwoData();

        ConvertData convert = new ConvertData();
        convert.printConvertData();


        Math krug = new Math();//Необходимо посчитать площадь круга с указанным радиусом с точностью 50 знаков после запятой

        krug.printPloshad();

        SummOf summ = new SummOf();//Даны три числа, необходимо ответить, является ли третье число суммой двух первых.
        summ.printEqquals();


        MinMax minMax = new MinMax();//Даны три числа. Нужно найти минимум и максимум не используя условный и тернарный операторы
        minMax.printMax();
    }

}
