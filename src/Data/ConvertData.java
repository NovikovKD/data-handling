package Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class ConvertData {
    static private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(" MMM dd, yyyy hh:mm:ss a",Locale.ROOT);
    private String dataTime = " Aug 12, 2016 12:10:56 PM";
    private LocalDateTime newData = LocalDateTime.parse(dataTime, formatter);




    public void printConvertData() {
        System.out.println( "Из Aug 12, 2016 12:10:56 PM получаем: " + newData.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }
}

