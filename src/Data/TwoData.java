package Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class TwoData {
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", Locale.ROOT);

    private String data = "11-04-2044";
    private String data2 = "05-11-1985";

    private LocalDate firstData = LocalDate.parse(data, formatter);
    private LocalDate secondData = LocalDate.parse(data2, formatter);
    private long FromTo = ChronoUnit.DAYS.between(secondData,firstData);


    public void printTwoData() {
        System.out.println("day from 05-11-1985 to 04-11-2044 : " + FromTo);
    }

}
